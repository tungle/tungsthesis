%


\documentclass[10pt,twocolumn,letterpaper]{article}

\usepackage{dicta}
\usepackage{times}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{subfig}

% Include other packages here, before hyperref.

% If you comment hyperref and then uncomment it, you should delete
% egpaper.aux before re-running latex.  (Or just hit 'q' on the first latex
% run, let it finish, and you should be clear).
\usepackage[pagebackref=true,breaklinks=true,letterpaper=true,colorlinks,bookmarks=false]{hyperref}


\dictafinalcopy % *** Uncomment this line for the final submission

\def\dictaPaperID{164 } % *** Enter the DICTA Paper ID here
\def\httilde{\mbox{\tt\raisebox{-.5ex}{\symbol{126}}}}

% Pages are numbered in submission mode, and unnumbered in camera-ready
\ifdictafinal\pagestyle{empty}\fi
\begin{document}

%%%%%%%%% TITLE
\title{Human Action Recognition from Boosted Pose Estimation}

\author{Li Wang\\
Najing Forestry University\\
No. 159 Longpan Road, Nanjing, China\\
{\tt\small wang.li.seu.nj@gmail.com}
% For a paper whose authors are all at the same institution,
% omit the following lines up until the closing ``}''.
% Additional authors and addresses can be added with ``\and'',
% just like the second author.
% To save space, use either the email address or home page, not both
\and
Li Cheng\\
Bioinformatics Institute\\
30 Biopolis Street, #07-01 Matrix, Singapore\\
{\tt\small wang.li.seu.nj@gmail.com}
}

\maketitle
% \thispagestyle{empty}

%%%%%%%%% ABSTRACT
\begin{abstract}

This paper presents a unified framework for recognizing human action in video using human pose estimation. Due to high variation of human appearance and noisy context background, accurate human pose analysis is hard to achieve and rarely employed for the task of action recognition. In our approach, we take advantage of the current success of human detection and view invariability of local feature-based approach to design a pose-based action recognition system. We begin with a frame-wise human detection step to initialize the search space for human local parts, then integrate the detected parts into human kinematic structure using a tree structural graphical model. The final human articulation configuration is eventually used to infer the action class being performed based on each single part behavior and the overall structure variation. In our work, we also show that even with imprecise pose estimation, accurate action recognition can still be achieved based on informative clues from the overall pose part configuration. The promising results obtained from action recognition benchmark have proven our proposed framework is comparable to the existing state-of-the-art action recognition algorithms.

\end{abstract}

%%%%%%%%% BODY TEXT
\section{Introduction}

Accurate analysis of human activities in video is becoming an attractive goal for computer vision research, due to its promising applications in surveillance and online multimedia mining. Nevertheless, the task remains challenging for years due to the complexity of realistic environment, variety of view and rate, and semantic representation of human activities.

There is a common trend in action recognition using sparse, informative feature points\cite{DolRabCotBel:VSPETS05}\cite{TuanLiJian:ICPR10}. These feature points are normally formed as 2D or 3D interest points, depending on whether time dimension is used.

Inspired by feature point-based approach, we propose to characterize human behaviors through the use of feature parts(see Figure \ref{fig:featureParts}). Feature parts can be represented by body parts under different configuration of scale, location, and orientation (SLO). Then the problem of recognizing human action becomes how to track and estimate different 2D human poses in video. Our work builds upon the pictorial structures model \cite{FelHut:ijcv05}~\cite{FerJimZis:cvpr08}~\cite{Ram:nips07} like 'stickman' of Figure ~\ref{fig:featureParts}.

\begin{figure}[t]
\begin{center}$
\begin{array}{ccc}
\includegraphics[width=0.3\linewidth]{../images/dicta2010/boxing.png} &
\includegraphics[width=0.3\linewidth]{../images/dicta2010/boxingPose.png} &
\includegraphics[width=0.09\linewidth]{../images/dicta2010/boxingFeature.png} \\
\includegraphics[width=0.3\linewidth]{../images/dicta2010/clapping.png} &
\includegraphics[width=0.3\linewidth]{../images/dicta2010/clappingPose.png} &
\includegraphics[width=0.2\linewidth]{../images/dicta2010/clappingFeature.png}
\end{array}$
\end{center}
   \caption{Feature extraction example. The first row shows a frame from boxing video, and from left to right: the frame, pose estimation, a stick model of the parts that form a pose. The second row presents a frame from clapping video.}
\label{fig:featureParts}
\end{figure}

 Tracking people kinematically is important and difficult. No current system is capable of kinematic tracking on a large scale, and most demonstrate results on mere hundreds of frames. The reason is that people can move very fast and configure themselves in many different poses. An alternative is to ignore motion continuity and find people's pose in each frame independently. In general, estimating pose is hard for people wear different clothes and can take on many poses. This suggests a bottom-up approach using part detectors ~\cite{MikSchZis:eccv04}~\cite{MoriRenEfros:cvpr04}~\cite{Ram:nips07}~\cite{MykhayloAndriluka:cvpr08}. We follow ~\cite{MykhayloAndriluka:cvpr08} to estimate pose and extend it to tracking pose in video.

Since a pose is represented by body parts with different location and orientation, we can first attempt to detect feature parts then easily infer on the final pose. Special attention is given to the arms and legs which carry most of the information necessary to distinguish different poses. Using human torso as a reference point, we compute relative location and orientation of arms and legs to the torso. Geometric information about the body parts is used as their recognition feature. Noticing the fact that different actions are carried out under different pose patterns, we develop a descriptor from poses to categorize the distinctive human actions in videos. Feature parts extracted from poses detected in training videos are clustered to form a dictionary of part prototypes. The only information kept from all subsequent video data is the type of the part prototypes present. Obviously these prototypes represent spacial relationship between parts. We argue that such a representation is sufficient for action recognition. In this approach we pay more attention to extract feature parts from 2D space. We can easily shift to 3D feature parts if we process feature parts in spacial-temporal space.

The structure of the paper is as follows. In Section 2 we discuss related works. We describe our framework in Section 3. In Section 4 we present the performance of our algorithm on KTH ~\cite{SchLapCap:icpr04} dataset. This paper is concluded in Section 5 with hints for future works in this direction.

\section{Related Work}

Our work focus on building a framework for action recognition including human detection, human tracking, human pose detection and action recognition. Human pose detection builds mainly on ~\cite{MykhayloAndriluka:cvpr08}, which use pictorial structures model and part detectors to detect pose of human. There are also some other works using pictorial structures model to detect human pose. ~\cite{FelzenszwalbHuttenlocher:IJCV05} first use pictorial structures to do pose reduction. ~\cite{Ram:nips07}~\cite{FerJimZis:cvpr08} use pictorial structures to parse human pose on CRF model.

To reduce the searching space before parsing human pose, we start by detecting human body in every frame, using a sliding window detection
based on Histograms of Oriented Gradients~\cite{DalalTriggs:cvpr08}, and associate detections over time. The detected result is a bounding box contained all body parts of a tracking person.

The hard part of human action recognition is how to represent human actions. There are various approaches to represent human actions. Trajectories of human have popularly been used as features to infer the activity of human. A good survey of the approach can be found in ~\cite{CedrasShah:95}~\cite{YilmazShah:CVIU08}. The shape of the human silhouette plays a very important role in recognizing human actions, and several methods ~\cite{BobickDavis.:pami01}~\cite{GorelickBlankShechtman:pami07}~\cite{KeSukHeb:iccv07} based on it, have been proposed to action recognition. Recently, spacetime interest points have emerged as a popular means for action classification ~\cite{SchLapCap:icpr04}~\cite{DolRabCotBel:VSPETS05}\cite{TuanLiJian:ICPR10}.

The feature for action recognition proposed in the present paper is high-level action descriptors from low-level features. Most closely related to the approach is from ~\cite{AliAggarwal:01}. The reason why there are rarely works using high-level features may be that it is hard to detect human pose accurately. We try to show that our framework can be competitive with other state-of-the-art methods.

\section{Framework for Action Recognition}

In the following sections we describe our framework in detail. In Section 3.1 we talk about people detection. We describe pose estimation in Section 3.2, and in Section 3.3 we describe action recognition based on feature parts.

\subsection{People Detection and Tracking}

Instead of tracking people using tracking technology~\cite{BreglerMalik:cvpr98}~\cite{IsardMacCormick:iccv01}~\cite{Rohr:cvpr93}~\cite{SidenbladhBlackFleet:eccv00}, we start by detecting person in each frame independently through a sliding window detection based on Histograms of Oriented Gradients(HOG)~\cite{DalalTriggs:cvpr08}, then associate detections over time. The sliding window is scanned across the frame at all positions and scales. Dalal pointed out in ~\cite{DalTri:cvpr05} that HOG detectors cue is mainly on silhouette contours(especially the head, shoulders and feet). So the detected result should provide the scale information of a person. Then we normalized different scale of results into a fixed scale. Given the possible locations of body and their scale, we can cut out much space in scale and spacial dimension.

\paragraph{HOG Detection}

We follow ~\cite{DalTri:cvpr05} to define a dense representation of an image at a particular resolution. The image is first divided into 8x8 non-overlapping pixel regions, or cells. For each cell, we accumulate a 1D histogram of gradient orientations over pixels in that cell. These histograms capture local shape properties but are also somewhat invariant to small deformations.

The gradient at each pixel is quantified into one of nine orientation bins, and each pixel "votes" for the orientation of its gradient, with a strength that depends on the gradient magnitude at that pixel. For color images, we compute the gradient of each color channel and pick the channel with highest gradient magnitude at each pixel. Finally, the histogram of each cell is normalized with respect to the gradient energy in a neighborhood around it. We look at the four $8\times 16$ blocks of cells that contain a particular cell and normalize the histogram of the given cell with respect to the total energy in each of these blocks. This leads to a $9\times 8\times 16$ dimensional vector representing the local gradient information inside a cell.

\paragraph{Temporal association}

After applying the HOG detection in every frame of a video, we need to get tracks by combining these detected bounding-box in every frame. Each track should connects the same person and have enough temporal continuity.
We follow ~\cite{FerJimZis:cvpr08} and treat temporal association as a grouping problem~\cite{SivicEveringhamZisserman:cvpr05}, where the elements to be grouped are bounding-boxes. For a similarity measurement, we use the area of the intersection divided by the area of the union (IoU), which subsumes both location and scale information, damped over time. We group detections based on these similarities using the Clique Partitioning algorithm of ~\cite{FerrariTuytelaarsGool:cvpr01}, under the constraint that no two detections from the same frame can be grouped.

\subsection{Pose Estimation}

The results of pose estimation can be used in activity recognition. Although pose estimation is a hard work due to the complexity of human body's pose, there is a simple model to describe it. We use pictorial structures in modeling human body. The pictorial structure is restricted to tree structure which can fit to many classes of objects. For human being, the connections between parts can form a tree structure corresponding to their skeletal structure. To reduce searching space, a part detector has been used to detect possible parts in the first step. Similar to the relationship between body parts, the variances of body parts in different frames should be restricted in a range when body parts changes smoothly over time. Combining the spatial and temporal parsing, we can estimate pose in a short time, even though body parts in some frames are ambiguous.

\subsubsection{Statistical Framework for Pose Estimation}
We follow ~\cite{MykhayloAndriluka:cvpr08} to model the human body which can be decomposed into a set of parts. Each part can be modeled as a rectangle parameterized by $(x,y,\theta,s)$. $(x,y)$ is the position of the rectangle in image coordinate. $\theta$ is the absolute part orientation, and $s$ is the part scale which is relative to the size of the part in the training set.

Assume the body can be decomposed $N$ parts denoted as $L={l_0,l_1,...,l_N}$. Given an image D, the posterior of the parts configuration is modeled as $p(L|D) \propto p(D|L)p(L)$, where $p(D|L)$ is the likelihood of the image evidence given a particular body part configuration, and $p(L)$ is the prior probability of a special configuration.


\paragraph{Part relationship} The part configurations $L$ are constrained by kinematic dependencies between parts. Mapping the kinematic structure on a directed tree graph and using bayes' analysis, the distribution of a configuration can be factorized as
\begin{align}
p(L)=p(l_0)\prod_{(i,j)\in E} p(l_i|l_j),
\label{eq:configurationLikelihood}
\end{align}

$E$ is the set of all directed edges in the kinematic tree and let $l_0$ be the root node which is torso. We assume that the prior for the root $p(l_0)$ is uniform.
We follow ~\cite{MykhayloAndriluka:cvpr08} to model the part relations using Gaussian distributions. It was pointed out in ~\cite{FelHut:ijcv05} that the relationship between parts can be captured well by a Gaussian distribution if they can be transformed into a same space.

\paragraph{Part model}
The posterior of the part configuration relies on a specific configuration. Assuming that a part evidence $d_i$ only depends on its own configuration $l_i$, each part evidence will be conditionally independent given its own configuration. The likelihood can be written as:

\begin{align}
p(D|L) = \prod_{(i=0)}^{N} p(d_i|L) = \prod_{(i=0)}^{N} p(d_i|l_i)
\label{eq:evidenceLikelihood}
\end{align}

Note here that the assumption is only justifiable when these parts do not affect each other such as occluding each other significantly. Combining $(~\ref{eq:configurationLikelihood})$ and $(~\ref{eq:evidenceLikelihood})$, the posterior over the configuration of parts can be factorized as:

\begin{align}
p(L|D) \propto  p(l_0).\prod_{(i=0)}^{N}p(d_i|l_i).\prod_{(i,j)\in E} p(l_i|l_j)
\label{eq:configurationPartsLikelihood}
\end{align}

\paragraph{Part probability}
We first use AdaBoost classifier to detect all possible positions of parts. The classifier is trained based on shape context ~\cite{BelongieMalikPuzicha:pami02} feature. The form of the classifier is:

\begin{align}
\textbf{h}(x) = \sum_{t=1}^{T}\alpha_t h_t(x)
\label{eq:configurationPartsLikelihood}
\end{align}

For AdaBoost training, each annotated part is scaled and rotated to a canonical pose prior to learning. It can significantly simplify the classification task. In detection, each part may be rotated to any direction. We quantize all directions to 24 directions. We use Adaboost to detect parts in each direction where we can rotate the image before detection as we did in the AdaBoost training.

To integrate the discriminative classifier into the probabilistic framework described above, it is need to get probability from $\textbf{h}(x)$. FriedMan ~\cite{Friedman00greedyfunction} proposes an elegant method for deriving boosting algorithms for a wide range of loss functions. If we use negative log likelihood as the loss function, we can get the probability in terms of $\textbf{h}(x)$. We define $p(x)$ as the probability of being object. Follow ~\cite{Friedman00greedyfunction}, we can define :

\begin{align}
p = \sigma(2h(x))
\label{eq:sigmoidLikelihood}
\end{align}

Where $\sigma(v)=\frac{1}{1+exp(-v)}$ is the sigmoid and $\sigma(v) \in [0,1]$. Integrating it into the probabilistic framework, we get:

\begin{align}
p(d_i|l_i) = \sigma(2h(x(l_i)))
\label{eq:sigmoidLikelihood1}
\end{align}

Where $x(l_i)$ denotes the feature for the part configuration $l_i$. Although it's natural to use $\sigma(v)$ to define probability, we follow ~\cite{MykhayloAndriluka:cvpr08} and interpret the normalized classifier margin as the likelihood:

\begin{align}
p(d_i|l_i) = \frac{\sum_t \alpha_{i,t}h_t(x(l_i))}{\sum_t \alpha_{i,t}}
\label{eq:marginLikelihood1}
\end{align}

\subsection{Pose for Human Action Representation}

\subsubsection{Feature Parts}

The body parts should have different configurations for different actions. Figure~\ref{Configuration of different Human Actions} shows some classic frames for six different actions:Hand Waving, Hand Clapping, Boxing, Walking, Jogging and Running. There is much difference among Hand Waving, Hand Clapping, Boxing and Jogging such as a person's hand moving from down to up in Hand Waving and hand moving from behind to front in Hand Clapping. But Walking, Jogging and Running have similar configurations. Especially for Jogging and Running, the differentiation between them is ambiguous. It is hard to distinguish them just by using the position and orientation of parts from them. We think the good feature to characterize the two kinds of actions is the rate at which a person execute them, but using the feature is beyond this paper's range. So we choose Hand Waving, Hand Clapping, Boxing and Jogging as a subset to experiment.

The position of each part can be represented as $(x,y,\theta)$ as we did in pose estimation. To normalize the position of each part, we use tors' position as a reference point and compute the relative positions of the other parts. The relative position of each part can be represented as $(\Delta x,\Delta y,\Delta \theta)$. From Figure~\ref{Configuration of different Human Actions}, we can see that not all results of detection are correct. The  reason may be that the appearance of some body parts merged in body's, and we can't achieve $100\%$ accuracy. But we only need to know some parts' position that provide the information of action's type.

\begin{figure}%
\centering
\subfloat[][]{%
\label{fig:ex3-a}%
\includegraphics[height=0.9in]{../images/dicta2010/wavingPose.png}}%
\hspace{8pt}%
\subfloat[][]{%
\label{fig:ex3-b}%
\includegraphics[height=0.9in]{../images/dicta2010/clappingPose.png}}\\
\hspace{8pt}
\subfloat[][]{%
\label{fig:ex3-c}%
\includegraphics[height=0.9in]{../images/dicta2010/boxingPose.png}}
\hspace{8pt}
\subfloat[][]{%
\label{fig:ex3-d}%
\includegraphics[height=0.9in]{../images/dicta2010/joggingPose.png}}
\caption[]{Using pose estimation to get different configuration for different Human actions:
\subref{fig:ex3-a} Hand Waving;
\subref{fig:ex3-b} Hand Clapping;
\subref{fig:ex3-c} Boxing;
\subref{fig:ex3-d} Jogging.
In the following experiment, we ignored the walking and running actions due to their similarity of pose configurations.}%
\label{Configuration of different Human Actions}%
\end{figure}


\subsubsection{Behavior Descriptor}

Inspired by interest points ~\cite{DolRabCotBel:VSPETS05} ~~\cite{Laptev:cvpr08} ~~\cite{FelMcARam:cvpr08}, each part can be thought as an interest part. After all parts of a body are detected, we can get the feature of each part and their geometry relationship. All these information construct the pose of human body and characterize a behavior.

Each part is represented as $(\Delta x,\Delta y,\Delta \theta)$. In a frame, we can compose all parts' information into a feature vector which can be used to recognize the action in this frame. In a video, we can also create a feature vector from all parts of a video, but it can increase the dimension of feature vector and decrease the tolerance of action rate. For this reason, we assigned each part a type by mapping it to the closest prototype vector. These part prototypes are created by clustering a large number of parts extracted from detected pose of training data. We cluster using the k-means algorithm. After mapping, the vector of feature is discarded and only their type is kept. The difference with interest points is that the interest points often discard the relative position information and the interest parts include the relative position as a behavior descriptor.

The feature vector of a part can not used to differentiate part type. To rich the feature of behavior descriptor, we classify the parts belong upper body as a type and classify the parts belong lower body as the other type. In the experiment, we found that it can improve the correct rate.

We use a histogram of part types as the behavior descriptor. Distance between the behavior descriptors (histograms) can be calculated by using the Euclidean or $\chi^2$ distance. One thing to note is that the behavior descriptor only reflect the distribution of parts in spacial space not temporal space. In the future, we can create 3D histogram in spacial-temporal space as behavior descriptor.


\section{Experiments}

\subsection{Dataset}
We choose KTH ~\cite{SchLapCap:icpr04} dataset. KTH has about 2400 grayscale video shots with 6 actions: boxing, handwaving, handclapping,
jogging, running, walking, performed by 25 persons under 4 different contexts and subdivided into 4 intervals. For the reason we described before, We use a subset which include boxing, handwaving, handclapping and jogging. Each person repeating each activity 4 times for about 4 seconds each in one scene, for a total of 400 clips. The clips have been sub-sampled to get 10 successive frames from a random position in the clip.

\subsection{Methodology}

For all the clips in KTH datset, we use leave one out cross validation to get an estimate of performance. At each run, the videos of 24 subjects are used for training, and the remaining subject is used for testing.

There is one parameter that need tuning. Because we need to create a library of part prototypes by clustering a large number of parts extracted from detected poses of training data. The number of clusters prototypes should affect the performance of algorithm. As can be seen in Figure ~\ref{fig:parameterROC}, the classification error is decreased as the number of clusters prototypes is increased when the number of clusters is less than 40. But when the number is increased beyond 40, the classification error is also increased. It can be explained that the more number of clusters prototypes the better the prototypes can represent the distribution of parts, but when the number of clusters is beyond some limit, the imprecise of part detection will disturb action recognition.

\begin{figure}[t]
\begin{center}
   \includegraphics[width=0.9\linewidth]{../images/dicta2010/parameterROC.jpg}
\end{center}
   \caption{We tested how the parameter setting about the number of clusters prototypes affect the performance of our method. The number of clusters prototypes is from 20 to 180.}
\label{fig:parameterROC}
\end{figure}

\subsection{Result}

For the KTH dataset we used leave one out cross validation to get the overall classification error. Confusion matrice for the four categories of behavior is shown in Figure \ref{fig:confusionMatrix}. The overall recognition accuracy is 90.00\%. Note that the result can't compared to other methods directly due to ignoring two classes by us. Rather, we want to show that the tree structure pose works well in action recognition. The temporal information haven't been used in recognition until now while most other methods have used temporal information. We believe that the recognition accuracy using tree structure pose will be improved further if temporal information is used.

\begin{figure}[t]
\centering
    \subfloat[]{
    \label{fig:confusion-a}
    \includegraphics[width=0.5\linewidth]{../images/dicta2010/confusionMatrix_1_knn.jpg}}
    \subfloat[]{
    \label{fig:confusion-b}
    \includegraphics[width=0.5\linewidth]{../images/dicta2010/confusionMatrix_1_svm.jpg}}
   \caption{Confusion matrix generated by two classifiers:\subref{fig:confusion-a} 1-nearest neighbor; \subref{fig:confusion-b} Support Vector Machines .}
\label{fig:confusionMatrix}
\end{figure}

\section{Conclusion}

The human pose can often be used in pose estimation, but it was unusual to be used in action recognition. The reason may be that the accuracy of pose estimation can not be high. In this work, we showed that human pose can be estimated well through our framework. Based on the result of pose estimation, feature parts can be created and used in action recognition. Although we only used spacial information, we still achieved comparable results. In the future, we want to incorporate temporal information, and use feature parts in spacial-temporal space.


{\small
\bibliographystyle{plain}
\bibliography{EventRecog}
}


\end{document}
