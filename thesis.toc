\select@language {english}
\contentsline {chapter}{List of Figures}{x}{chapter*.2}
\contentsline {chapter}{List of Tables}{xiii}{chapter*.3}
\contentsline {chapter}{List of Algorithms}{xvi}{chapter*.4}
\contentsline {chapter}{Nomenclature}{xvi}{chapter*.4}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}The problem}{1}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Solutions for reducing travel delay}{2}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Solutions for reducing fuel consumption and CO$_2$\ emissions}{3}{subsection.1.1.2}
\contentsline {section}{\numberline {1.2}The purpose of this doctoral study}{4}{section.1.2}
\contentsline {section}{\numberline {1.3}Our hypotheses}{4}{section.1.3}
\contentsline {section}{\numberline {1.4}Structure of the thesis}{5}{section.1.4}
\contentsline {section}{\numberline {1.5}List of publications}{6}{section.1.5}
\contentsline {section}{\numberline {1.6}List of contributions}{7}{section.1.6}
\contentsline {chapter}{\numberline {2}Literature Review}{9}{chapter.2}
\contentsline {section}{\numberline {2.1}Traffic signal control systems}{9}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Terminology}{10}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Objective and control variables}{12}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Classification of control methods}{12}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}Fixed-time isolated methods }{13}{subsection.2.1.4}
\contentsline {subsubsection}{\numberline {2.1.4.1}Stage-based method}{13}{subsubsection.2.1.4.1}
\contentsline {subsubsection}{\numberline {2.1.4.2}Phase-based methods}{14}{subsubsection.2.1.4.2}
\contentsline {subsection}{\numberline {2.1.5}Fixed-time coordinated method}{15}{subsection.2.1.5}
\contentsline {subsubsection}{\numberline {2.1.5.1}MAXBAND}{15}{subsubsection.2.1.5.1}
\contentsline {subsubsection}{\numberline {2.1.5.2}TRANSYT}{16}{subsubsection.2.1.5.2}
\contentsline {subsubsection}{\numberline {2.1.5.3}Advantages and disadvantages of fixed-time methods}{18}{subsubsection.2.1.5.3}
\contentsline {subsection}{\numberline {2.1.6}Traffic-responsive methods}{18}{subsection.2.1.6}
\contentsline {subsubsection}{\numberline {2.1.6.1}Traffic-responsive isolated methods}{19}{subsubsection.2.1.6.1}
\contentsline {subsection}{\numberline {2.1.7}Traffic-responsive coordinated methods}{20}{subsection.2.1.7}
\contentsline {subsubsection}{\numberline {2.1.7.1}Library-lookup method}{21}{subsubsection.2.1.7.1}
\contentsline {subsubsection}{\numberline {2.1.7.2}Online optimised methods}{21}{subsubsection.2.1.7.2}
\contentsline {section}{\numberline {2.2}A summary of signal control methods}{25}{section.2.2}
\contentsline {section}{\numberline {2.3}Eco-driving methods based on traffic signal information}{29}{section.2.3}
\contentsline {section}{\numberline {2.4}Related technology}{34}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Vehicle-to-Infrastructure communication}{34}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Adaptive cruise control}{36}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}Vehicle positioning techniques}{36}{subsection.2.4.3}
\contentsline {section}{\numberline {2.5}Summary}{38}{section.2.5}
\contentsline {chapter}{\numberline {3}Approximate Dynamic Programming}{40}{chapter.3}
\contentsline {section}{\numberline {3.1}MDP}{41}{section.3.1}
\contentsline {section}{\numberline {3.2}DP}{42}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Value iteration}{44}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Policy iteration}{46}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}ADP}{47}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Roll-out ADP}{47}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}M-step scheduling and simulated trajectories}{48}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Parametric ADP}{49}{subsection.3.3.3}
\contentsline {subsection}{\numberline {3.3.4}Combination of roll-out and parametric forms of ADP}{52}{subsection.3.3.4}
\contentsline {section}{\numberline {3.4}Summary}{54}{section.3.4}
\contentsline {chapter}{\numberline {4}Applying ADP into Traffic-light Scheduling Problem}{55}{chapter.4}
\contentsline {section}{\numberline {4.1}Related ADP-based methods}{55}{section.4.1}
\contentsline {section}{\numberline {4.2}Problem formulation}{58}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Assumptions}{58}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}The state space}{60}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Control and Dynamics}{62}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Step cost and optimisation objective}{63}{subsection.4.2.4}
\contentsline {subsection}{\numberline {4.2.5}Linear features}{64}{subsection.4.2.5}
\contentsline {section}{\numberline {4.3}Control policy}{65}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Control Algorithm}{65}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Importance of turn-flow information}{67}{subsection.4.3.2}
\contentsline {section}{\numberline {4.4}Evaluation of the approach}{68}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Nagel-Schreckenberg traffic model}{68}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Modified NaSch model and simulator}{70}{subsection.4.4.2}
\contentsline {subsection}{\numberline {4.4.3}Network topologies}{71}{subsection.4.4.3}
\contentsline {subsection}{\numberline {4.4.4}Traffic demand scenarios}{73}{subsection.4.4.4}
\contentsline {subsection}{\numberline {4.4.5}Traffic signal settings}{74}{subsection.4.4.5}
\contentsline {subsection}{\numberline {4.4.6}Benchmarking method}{75}{subsection.4.4.6}
\contentsline {subsection}{\numberline {4.4.7}Experimental result}{76}{subsection.4.4.7}
\contentsline {subsubsection}{\numberline {4.4.7.1}Moderate demand}{76}{subsubsection.4.4.7.1}
\contentsline {subsubsection}{\numberline {4.4.7.2}Heavy traffic}{79}{subsubsection.4.4.7.2}
\contentsline {subsubsection}{\numberline {4.4.7.3}Time-dependent demand}{80}{subsubsection.4.4.7.3}
\contentsline {subsubsection}{\numberline {4.4.7.4}Large-scale network (scenarios 5 and 6)}{81}{subsubsection.4.4.7.4}
\contentsline {subsection}{\numberline {4.4.8}Evolutions of approximation}{82}{subsection.4.4.8}
\contentsline {section}{\numberline {4.5}Summary}{83}{section.4.5}
\contentsline {chapter}{\numberline {5}Speed advising system}{86}{chapter.5}
\contentsline {section}{\numberline {5.1}Assumptions and Terms}{87}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Assumptions}{87}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Terms}{88}{subsection.5.1.2}
\contentsline {section}{\numberline {5.2}Fuel consumption and emission modelling}{89}{section.5.2}
\contentsline {section}{\numberline {5.3}Speed planning method}{91}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Effective speed adaptation}{92}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}``Fixed-time plus'' controller}{93}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}No-slow-down zone (NSDZ)}{97}{subsection.5.3.3}
\contentsline {section}{\numberline {5.4}Test road network}{98}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Network topologies}{98}{subsection.5.4.1}
\contentsline {subsection}{\numberline {5.4.2}Traffic signal settings}{99}{subsection.5.4.2}
\contentsline {subsection}{\numberline {5.4.3}Signal schedules}{100}{subsection.5.4.3}
\contentsline {subsection}{\numberline {5.4.4}Traffic demand}{101}{subsection.5.4.4}
\contentsline {section}{\numberline {5.5}Experimental result and analysis}{101}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}Vehicle types}{101}{subsection.5.5.1}
\contentsline {subsection}{\numberline {5.5.2}One car on empty roads}{102}{subsection.5.5.2}
\contentsline {subsection}{\numberline {5.5.3}One car in traffic}{103}{subsection.5.5.3}
\contentsline {subsection}{\numberline {5.5.4}Mass-scale test}{105}{subsection.5.5.4}
\contentsline {subsection}{\numberline {5.5.5}Fuel consumption }{105}{subsection.5.5.5}
\contentsline {subsection}{\numberline {5.5.6}CO$_2$\ emission}{108}{subsection.5.5.6}
\contentsline {subsection}{\numberline {5.5.7}Delay and stops}{110}{subsection.5.5.7}
\contentsline {subsection}{\numberline {5.5.8}Limitations}{112}{subsection.5.5.8}
\contentsline {section}{\numberline {5.6}Summary}{113}{section.5.6}
\contentsline {chapter}{\numberline {6}A fully cooperative control system}{115}{chapter.6}
\contentsline {section}{\numberline {6.1}Assumptions and Terms}{115}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Assumptions}{116}{subsection.6.1.1}
\contentsline {subsection}{\numberline {6.1.2}Terms}{116}{subsection.6.1.2}
\contentsline {section}{\numberline {6.2}Methodology}{117}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Problem formulation revisited}{117}{subsection.6.2.1}
\contentsline {subsubsection}{\numberline {6.2.1.1}State space}{117}{subsubsection.6.2.1.1}
\contentsline {subsubsection}{\numberline {6.2.1.2}Control}{118}{subsubsection.6.2.1.2}
\contentsline {subsubsection}{\numberline {6.2.1.3}Dynamics}{119}{subsubsection.6.2.1.3}
\contentsline {subsubsection}{\numberline {6.2.1.4}Step cost and optimisation objective}{120}{subsubsection.6.2.1.4}
\contentsline {section}{\numberline {6.3}ADP}{121}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Approximation feature}{121}{subsection.6.3.1}
\contentsline {subsection}{\numberline {6.3.2}M-step scheduling}{122}{subsection.6.3.2}
\contentsline {subsection}{\numberline {6.3.3}Suggested action}{123}{subsection.6.3.3}
\contentsline {subsection}{\numberline {6.3.4}Control Algorithm}{124}{subsection.6.3.4}
\contentsline {section}{\numberline {6.4}Experimental results and analysis}{125}{section.6.4}
\contentsline {subsection}{\numberline {6.4.1}Sensitivity of $k$}{125}{subsection.6.4.1}
\contentsline {subsection}{\numberline {6.4.2}Stops}{130}{subsection.6.4.2}
\contentsline {subsection}{\numberline {6.4.3}Delay by our system}{131}{subsection.6.4.3}
\contentsline {subsection}{\numberline {6.4.4}Objective value}{133}{subsection.6.4.4}
\contentsline {subsection}{\numberline {6.4.5}Fuel consumption}{133}{subsection.6.4.5}
\contentsline {subsection}{\numberline {6.4.6}CO$_2$\ emissions}{136}{subsection.6.4.6}
\contentsline {subsection}{\numberline {6.4.7}Other emissions}{139}{subsection.6.4.7}
\contentsline {subsection}{\numberline {6.4.8}Benchmark system Fplus}{139}{subsection.6.4.8}
\contentsline {subsection}{\numberline {6.4.9}Comparison to Fplus\_0\%}{140}{subsection.6.4.9}
\contentsline {subsection}{\numberline {6.4.10}Comparison to Fplus with smart cars}{144}{subsection.6.4.10}
\contentsline {subsubsection}{\numberline {6.4.10.1}Delay and stops}{144}{subsubsection.6.4.10.1}
\contentsline {subsubsection}{\numberline {6.4.10.2}Fuel consumption and CO$_2$\ emissions}{147}{subsubsection.6.4.10.2}
\contentsline {section}{\numberline {6.5}Is it worthwhile?}{148}{section.6.5}
\contentsline {section}{\numberline {6.6}Summary}{151}{section.6.6}
\contentsline {chapter}{\numberline {7}Conclusion and future research directions}{153}{chapter.7}
\contentsline {section}{\numberline {7.1}Summary}{154}{section.7.1}
\contentsline {section}{\numberline {7.2}Future works and research directions}{155}{section.7.2}
\contentsline {subsection}{\numberline {7.2.1}Using a continuous car-movement model}{155}{subsection.7.2.1}
\contentsline {subsection}{\numberline {7.2.2}Extending network size}{156}{subsection.7.2.2}
\contentsline {subsection}{\numberline {7.2.3}Bridging signalized intersection and completely non-signalized intersection}{156}{subsection.7.2.3}
\contentsline {chapter}{\numberline {A}Correction of a published paper}{157}{appendix.A}
\contentsline {chapter}{References}{159}{appendix*.87}
