\chapter{Related Fields}
\ifpdf
    \graphicspath{{Chapter1/Chapter1Figs/PNG/}{Chapter1/Chapter1Figs/PDF/}{Chapter1/Chapter1Figs/}}
\else
    \graphicspath{{Chapter1/Chapter1Figs/EPS/}{Chapter1/Chapter1Figs/}}
\fi

Here given some works related to Object and Activity Recognition, though the categories are not yet complete, they give a little idea about how those works are joint and related. Eventually, when those areas are better reviewed, hopefully there will be better organization of the texts.

% ------------------------------------------------------------------------
\newpage
\section{Signal Processing}
Since image and video can be represented as a set of pixel values, their contents can be analyzes as combination of signal functions. Those algorithm in Signal Processing might be helpful for few tasks in Computer Vision, particularly with Feature Detection and Extraction.

\subsection{Adaptive Digital Filter}
\cite{bellanger:CRC01} work is about Adaptive Digital Filters, which describes the two fields Adaptive Filtering and Signal Analysis.

Adaptive techniques aim for accuracy and flexibility. Understanding the background theory behind is essential, mere application will be hard. Learning adaptive filtering goes with learning signal analysis.

\subsubsection{Signal Analysis}
Signal carries information from a source to a receiver. In real world, several signals are transmitted and processed together, as described in Figure \ref{fig: A Transmission system of order N} and Equation \ref{eq:signal linear combination}.

\begin{eqnarray}
  y_{i} = \sum^{N-1}_{j=0}m_{ij}x_{j}, \hspace*{0.5in} 0 \leqslant i \leqslant N - 1
  \label{eq:signal linear combination}
\end{eqnarray}

\begin{figure}[!htb]
  \begin{center}
    \includegraphics[width=3in]{1_1transmissionsystem}
    \caption{A Transmission system of order N \cite{bellanger:CRC01}}
    \label{fig: A Transmission system of order N}
  \end{center}
\end{figure}

The problem now becomes how to find the source variables $X$ using the receiver information $Y$, also the transmission coefficients $M$. Equation \ref{eq:signal linear combination} can be rewritten in matrix form:

\begin{eqnarray}
  Y = MX
\end{eqnarray}

with

\[
  X = \begin{bmatrix}
      x_{0} \\
      x_{1} \\
      \vdots \\
      x_{N-1}
    \end{bmatrix},
  Y = \begin{bmatrix}
      y_{0} \\
      y_{1} \\
      \vdots \\
      y_{N-1}
    \end{bmatrix},
  M = \begin{bmatrix}
      m_{00} & m_{01} & \cdots & m_{0N-1} \\
      m_{10} & m_{11} & \cdots & m_{1N-1} \\
      \vdots &        &        & \vdots   \\
      m_{N-10} & \cdots & \cdots & m_{N-1N-1}
    \end{bmatrix}
\]

\subsubsection{Adaptive Filtering}
Together with signal analysis, the adaptive filter idea can be included during the calculation using one reference signal (which is supposed to be benchmark), as in Figure \ref{fig:Princicple of Adaptive Filter}

\begin{figure}[!htb]
  \includegraphics[width=6in]{1_2adaptivefilter}
  \caption{Principle of Adaptive Filter \cite{bellanger:CRC01}}
  \label{fig:Princicple of Adaptive Filter}
\end{figure}

In general, adaptive filtering tries the above calculation taking into account with the input signal a reference signal, which together yield an error value of the output. The cost function can then be established based on minimizing error value. Basically this book is more on theoretical side of signal processing and not easy to be consumed at all. However, \textbf{\emph{the idea of adaptive filtering and signal transmission between uncorrelated sources and receiver are worth being aware of}}.

% ------------------------------------------------------------------------
\newpage
\section{Manifolds}

This differential geometric technique owns its origin from topology analysis, and currently earning its popularity in shape matching.

\subsection{Learning and Matching of Dynamic Shape Manifolds for Human Action Recognition}
\cite{wang:TIP07}

% ------------------------------------------------------------------------
\newpage
\section{Posture Analysis}

These are the popular works in Posture analysis, which is quite temporal based for Action Recognition

\subsection{Designing a Posture Analysis System with Hardware Implementation}
\cite{coutinho:VLSI07}

% ------------------------------------------------------------------------
\newpage
\section{Relationship Match}

This includes work on matching internal relationship between features of the action.

\subsection{Spatio-Temporal Relationship Match:
Video Structure Comparison for Recognition of Complex Human Activities}
\cite{ryoo:iccv09}

% ------------------------------------------------------------------------
\newpage
\section{Feature Search}

Those works on searching for similar or distinctive features within the action regions.

\subsection{Discriminative subvolume search for efficient action detection}
\cite{yuan:cvpr09}

% ------------------------------------------------------------------------
\newpage
\section{Undirected Graphical Models}
\label{Undirected Graphical Models}

This section will discuss some fundamental theories and implementations using UGM. The essentials of this work will be to get familiarized with the implementation of a machine learning theory.

\subsection{Cheating Students Scenario}
\label{Cheating Students Scenario}

This is a very simple demonstration of UGM and its implementation. In this example, we have 4 nodes, 3 edges, 2 states of getting the answer right or wrong. There is one concept called potential, which has node and edge potential, in turns the product of all corresponding potentials. Another concept is probability, derived from potentials, dividing by the normalization constant Z, the sum of all possible potentials.

\begin{figure}[!htb]
  \includegraphics[width=4in]{small.pdf}
  \caption{Students sitting next to each other in the test}
  \label{fig:Students sitting next to each other in the test}
\end{figure}

\begin{equation}
\label{eq:potential}
    pot(x_{1},x_{2},...,x_{N}) = \prod^{N}_{i=1}\phi_{i}(x_{i})\prod^{E}_{e=1}\phi_{e}(x_{e_{j}},x_{e_{k}})
\end{equation}

\begin{equation}
\label{eq:crf_normalization}
    Z = \sum_{x_{1}} \sum_{x_{2}}\cdot\cdot\cdot\sum_{x_{n}} \prod^{N}_{i=1}\phi_{i}(x_{i})\prod^{E}_{e=1}\phi_{e}(x_{e_{j}},x_{e_{k}})
\end{equation}

\begin{equation}
\label{eq:probability}
    p(x_{1},x_{2},...,x_{N}) = \frac{1}{Z} \prod^{N}_{i=1}\phi_{i}(x_{i})\prod^{E}_{e=1}\phi_{e}(x_{e_{j}},x_{e_{k}})
\end{equation}

\textbf{Decoding} is the task to find the most possible configuration, the one with highest probability, can be done with $UGM\_Decode\_Exact$

\textbf{Inference} is the task to find the marginal probability of an individual node or edge taking the state, as well as the normalization, can be done with $UGM\_Infer\_Exact$

\textbf{Sampling} is the task to sample the configurations based on their probabilities, can be done with $UGM\_Sample\_Exact$

This example shows how the UGM toolbox can be used, also describes the concept of using potentials based on graphical models to calculate the probability.

\subsection{Chain UGM}
\label{Chain UGM}

This demonstration shows the chain example of UGM. Chain structure has Markov independence properties that given the neighboring nodes, each node in the chain is independent of the rest of the nodes. This chain structure is represented through a 'CS Grad Game of Life' example.

\subsubsection{CS Grad Game of Life}
\label{CS Grad Game of Life}

This is an example of Markov chain, illustrating how career decision of a CS Grad changes over time, with 7 states representing different jobs, 60 nodes are the number of years for consideration. The graphical model is similar to previous one, but with much larger nodes, with larger consideration, and infeasible to calculate by hand. There are few things that need to be defined with a Markov chain, the initial state assignment probability, the transition matrix from one node to another (or the edge potential), in this case the edge potential is set to be similar for all edges, but not symmetric (unlike the one in previous example).

\textbf{Decoding} this task is done using the Viterbi algorithm, again, the task is to find the configuration (60 state assignment for all chain nodes), theoretically, there should be $7^{60}$ state assignment possibilities, which makes it impractical to calculate them all. The idea is to implement Dynamic Programming, which is embedded in Viterbi algorithm. This involves one forward step, which calculates from start to end the best state assignment at each node, save the best state, and move on to the next, all the nodes after the current node are not considered in optimization, this help to reduce the calculation step to polynomial. The backward step backtracks all the best state assignment at each node, and derives the Viterbi path, or optimal state assignment. With UGM, this task can be done with $UGM\_Decode\_Chain$.

\textbf{Inference} is still the process to calculate marginalization probability. A method called Chapman-Kolomogorv can be used to infer the marginalization probability, but it's not generic for all kinds of chain, will work for Markov chain with proper probability encoded (sum to one). The algorithm used is called forward-backward, with the forward step similar to Viterbi algorithm, calculating the marginal value instead. The backward step calculates the sum of all marginalization, and derive the ratio, which is the actual marginalization. There are several ways to implement this forward-backward algorithm, but the idea is to reuse dynamic programming and focus on marginalization. With UGM, this task can be done with $UGM\_Infer\_Chain$.

\textbf{Sampling} is run to see how the model reacts to data. The idea is to sample next state at each current stage using the initials and transitions. This can be done using the algorithm called forward-filter backward-sample, in UGM, it is implemented as $UGM\_Sample\_Chain$.

\subsubsection{Other Exact Algorithm}
\label{Other Exact Algorithm}

Tree is a kind of undirected graphical model with one root and many branches, a tree can be solved by extending the chain techniques, giving multiple roots

% ------------------------------------------------------------------------


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../thesis"
%%% End:
