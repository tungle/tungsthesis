

\chapter{Approximate Dynamic Programming}
\label{chap1}
\ifpdf
    \graphicspath{{Chapter1/}{Chapter1/Chapter1Figs/PNG/}{Chapter1/Chapter1Figs/PDF/}{Chapter1/Chapter1Figs/}}
\else
    \graphicspath{{Chapter1/Chapter1Figs/EPS/}{Chapter1/Chapter1Figs/}}
\fi

\begin{spacing}{1.5}
When scheduling traffic signals adaptively, decisions are typically
produced online based on the current traffic state.  Although the decisions are made on the spot, the goal is to optimise the long-term objective, such as travel time, stops or fuel consumption. In adaptive decision making problems, we can approximate the decision process by formulating the problem as a Markov Decision Process (MDP).
A solution to an MDP can be found with Dynamic Programming (DP). However, due to the curse of dimensionality, a DP algorithm can only be used to solve problems with a small state space.  Consequently,  DP algorithms are impractical for real-time problems, such as the traffic signal scheduling,  where the dimension of the state space is large.  In real-time systems, both the responsiveness of the scheduler and the optimality of the schedule are important.  Trading optimality for responsiveness is often acceptable.  Hence, the system may produce suboptimal solutions in a practical amount of  time. Such solutions can be produced by Approximate Dynamic Programming (ADP). 

In this chapter, MDP, DP and ADP are outlined. All the material presented in this chapter is reproduced from the literature. The discussion of MDP is based on~\citep{Puterman94}, DP 
%and Reinforcement learning 
on~\citep{russel03modern} and ADP on~\citep{Sutton1998,Bertsekas2007}



%\footnote{the continuous-time concept here means the real-life time, which can be discretized into time steps. It is different from the discrete-time concept, which counts the number of reoccurences of an event}

\section{MDP}
In an MDP, successive decisions are made in order to either minimise cost or maximise reward in the long run.  The period of time can be finite or infinite. In this work, the concept of an MDP is restricted to continuous-time, discrete-state MDPs.  Other forms of MDPs can be found in~\citep{Puterman94}. Formally, an MDP is composed of four components: 
\begin{description}
	\item[$\stateSet$:] a set of system states. Each state in the system is denoted by $\state$.
	\item[$\ctrlSet_\state,\forall \state \in \stateSet$:] a set of possible or allowable decisions corresponding to a specific state. To simplify notation, we use $\ctrlSet$ for $\ctrlSet_\state$ when obvious that the decisions are constrained in $\ctrlSet_\state$.
	\item[$\tranSet(\nst|\action,\cst)$:] the probability that the system moves to next state $\nst$ if the current state is $\cst$ and action $\action$ is performed.
	\item [$\cost(\state,\action)$:] the cost incurred if the current state is $\state$ and action $\action$ is performed.
\end{description}
At each time step $t$, the controller has to produce a decision $\ctrl_t$ such that the total discounted cost 
\begin{equation}
E \{ \sum_{t=0}^\infty{\dis^t\cost(\state_t,\action_t)} \},
\label{eqUltimateCost}
\end{equation}
is minimised. Here,  $\dis$ is the discount factor that accounts for the uncertainty of the future. A current certain cost is valued higher than a future uncertain cost~\citep{Powell07Chap2}. An important property of an MDP is that the decision or cost is dependent only on the current state, not on past decisions or cost. This is called the Markov property. To find the solution to an MDP problem, we can use either linear programming or DP. In the scope of this work, we are only interested in DP.


\begin{figure}%
\includegraphics[width=4in]{MDP_rewardAction}%
\caption[An MDP with 3 states]{An MDP with 3 states (round nodes, numbered 1 to 3). State 1 and state 2 has one legitimate action each (oval-shaped nodes). State 3 has two legitimate actions (a1, a2). The cost of each action is immediately below the action node. The arrows heading out of action node bear the transition probabilities.}%
\label{MDPSystem}%
\end{figure}


\section{DP}
\label{chap1secDP}
The solution to an MDP problem is a mapping from action to state, called a \emph{policy}: $\mu = \ctrl(\state): \stateSet \rightarrow \ctrlSet $. The policy that yields the minimum expected total discounted cost in equation (\ref{eqUltimateCost}) is the optimal policy. An optimal policy can be found with DP. The key idea of DP is that a subproblem is solved first.  The algorithm then extends backwards to form and solve a larger subproblem. The extension continues until the subproblem becomes the original problem, and the solution to the problem is found. In DP, a value function $\val: \stateSet \rightarrow \mathbb{R}$ is used to record the quality of the system's state. 
$\val(\cstate)$ is defined as
\begin{eqnarray}
\val(\cstate) & := & \min_{\ctrl \in \ctrlSet} \{ {\cost(\cstate,\ctrl) + \dis \sum_{\nstate}\tranSet(\nstate|\action,\cstate)  \val(\nstate) \} } \label{eqStateVal1}, \\
&= & \min_{\ctrl \in \ctrlSet} \Bigl\{ {\cost(\cstate,\ctrl) + \dis E \{ \val(\nstate)\} \Bigr\} }.
\label{eqStateVal}
\end{eqnarray}
The value of $\val(\cstate)$ can be understood as the ``cost-to-go'' from state $\cstate$. Let $\val^\ast$ denote the true minimum cost of a state. Then we have
\begin{equation}
\val^\ast(\cstate) =  \min_{\ctrl \in \ctrlSet} \Bigl\{ {\cost(\cstate,\ctrl) + \dis E \{ \val^\ast(\nstate)\} \Bigr\} }.
\label{eqStateMinVal}
\end{equation}

Any sequence of control actions for a sequence of corresponding states is a policy. In the hope of finding the optimal policy, DP  records just the action that minimises the right term of equation (\ref{eqStateVal}). Let us call such a policy $\pi$:
\begin{equation}
\mu^\pi(\cstate) = arg \min_{\ctrl \in \ctrlSet} \Bigl\{ {\cost(\cstate,\ctrl) + \dis E \{ \val(\nstate)\} \Bigr\} }. 
\label{eqPolicy}
\end{equation}
If we let $\mu^*$ denote the optimal policy, then it has been proved that: 
\begin{equation}
\mu^\ast(\cstate) = arg \min_{\ctrl \in \ctrlSet} \Bigl\{ {\cost(\cstate,\ctrl) + \dis E \{ \val^\ast(\nstate)\} \Bigr\} }.
\label{eqOptimPolicy}
\end{equation}

Equation \eref{eqOptimPolicy} is Bellman's equation, which expresses that the optimality of $\mu^\ast(\nstate)$ is not altered by the action selected at $\cstate$. 
Once we have the optimal value of $\nstate$ corresponding to the optimal action $\mu^\ast\left(\nstate\right)$,  the task of choosing the optimal action at state $\cstate$ is equivalent to finding the action that minimises the sum of transition cost from $\cstate$ to $\nstate$ and the cost of $\nstate$. 

The challenge arising from Bellman's equation is to compute the optimal cost, or optimal corresponding action, for the state following $\nst$. Given a finite MDP, we can solve recursively from the last state. Given an infinite MDP, the system ceaselessly loops through its state set. However, if $\dis \in(0,1)$ and equation \eref{eqStateVal} is applied infinitely, then the state values will converge to the minimum value~\citep{Bellman57}. Two algorithms can be applied  to achieve the convergence: value iteration  and policy iteration.

\subsection{Value iteration}
In value iteration, equation \eref{eqStateVal} is applied iteratively. In each iteration, the algorithm starts with the given state, chooses the action that minimises the right-hand side of equation \eref{eqStateVal} and then updates each state's value with the newly computed value. The stopping condition can be either  a maximum number of iterations or when state values do not vary more than a threshold. 
Let $i$ denote the current iteration.  The action chosen is:
\begin{equation}
\action_i(\cstate)=  arg\min_{\ctrl \in \ctrlSet} \Bigl\{ {\cost(\cstate,\ctrl) + \dis E \{ \val_{i-1}(\nstate)\} \Bigr\} }
\label{eqStateValIterAction},
\end{equation}
and the value update is
\begin{equation}
\val_i(\cstate)=  \min_{\ctrl \in \ctrlSet} \Bigl\{ {\cost(\cstate,\ctrl) + \dis E \{ \val_{i-1}(\nstate)\} \Bigr\} } .
\label{eqStateValIter}
\end{equation}


\begin{table}%
\centering
\begin{tabular}[center]{c| c c c}
Iteration & \multicolumn{3}{c}{Node values}\\
&1&2&3 \\ \hline
1&0.0000&0.0000&0.0000\\
2&2.0000&2.0000&1.0000\\
3&3.0000&2.6000&1.8500\\
4&3.3200&3.0000&2.3275\\
5&3.5160&3.2310&2.5111\\
6&3.6298&3.3276&2.6073\\
7&3.6789&3.3757&2.6615\\
8&3.7030&3.4022&2.6868\\
9&3.7161&3.4150&2.6991\\
10&3.7225&3.4211&2.7055\\
11&3.7256&3.4243&2.7087\\
12&3.7272&3.4259&2.7103\\
13&3.7280&3.4267&2.7111\\
14&3.7284&3.4271&2.7115\\
15&3.7286&3.4273&2.7117\\
16&3.7287&3.4274&2.7118\\
17&3.7288&3.4274&2.7118\\
18&3.7288&3.4275&2.7118\\
19&3.7288&3.4275&2.7119\\
20&3.7288&3.4275&2.7119\\
21&3.7288&3.4275&2.7119\\
\end{tabular}
\caption{An example of value iteration.  Convergence occurs after the 19$^{\textrm{th}}$} iteration.
\label{ValIterTable}
\end{table}

Table \ref{ValIterTable} shows the algorithm in action for the example model in Figure \ref{MDPSystem}. The values converge after 19 iterations.

Here we update the value of $\cstate$ in one iteration based on the value of $\nstate$ in the previous iteration. However, the update strategy can vary, which leads to various versions of DP.  For example, in prioritised sweeping some chosen states are updated more frequently than the others, and the iteration of the following state, $\nstate$, may be greater than the iteration of current state $\cstate$.

\subsection{Policy iteration}\label{secC1PolIter}
In policy iteration, the algorithm starts off with an arbitrary policy. The policy is kept fixed, a fixed action for a state, until the state values converge. Then, a policy improvement is performed. The stopping condition can either be an upper bound on the number of iterations or when policy does not vary over iterations.  Let $\pi$ denote the current policy.  Policy iteration is performed through the equation: 
\begin{equation}
\val^\pi(\cstate)=   \cost(\cstate,\ctrl^\pi) + \dis E \{ \val^\pi(\nstate)\}  .
\label{eqStatePolIter}
\end{equation}
The policy improvement is done via an equation similar to \eref{eqStateValIterAction}, reformulated as follows: 
\begin{equation}
\mu^{\pi+1}(\cstate)=  arg\min_{\ctrl \in \ctrlSet} \Bigl\{ {\cost(\cstate,\ctrl) + \dis E \{ \val^{\pi}(\nstate)\} \Bigr\} }.
\label{eqStatePolImprov}
\end{equation}

In both value iteration and policy iteration, the state values are guaranteed to converge and are used to find the optimal policy. However, both methods require a model of the environment, i.e., the transition probability $\tranSet(\nstate|\cstate,\action)$. In reality, there are many problems where the model is either unknown or hard to compute. To overcome this difficulty, we use Monte Carlo simulation.

Each iteration in DP has a complexity of $\mathbb{O}(|S|^2|U|)$, which is a polynomial in the number of states. Formally, DP requires an infinite number of iterations for state values and policy to converge.  In practice, convergence tends to happen much faster, or we stop when the difference between iterations is within an acceptable range. $U$ can be quite small due to constraints on actions. However, the real challenge is with the state space, not the iteration. The state space grows exponentially with the number of state variables. For example, if we define a state vector $\state = (\state^1,\state^2,...,\state^Z)$, then each $\state^i$ can take L values and the state space is $L^Z$. This is called the curse of dimensionality. This drawback makes DP impractical for solving many realistic problems. Alternatively, we use APD.
%
%In the next sections, cures to these two problems will be presented. To overcome the first problem, a model-free algorithm must be used. One such algorithm (precisely one class of such algorithms) is Reinforcement Learning \citep{Sutton1998}. 
%To tackle this problem, we use APD. 

%\section{Reinforcement learning}
%\label{secRL}
%According to \citep{Sutton1998}, in reinforcement learning, ``the learner is not told which actions to take, as in most forms of machine learning, but instead must discover which actions yields the most reward by trying them''. Central to reinforcement learning is the idea that the agent (or in our case, the controller) has to learn by interacting with the environment. The goal is to find the true value of a state or the closest to it. The principle is that the controller makes one control action for its one state, finds out the cost of such action, and what state it ends up in, then it tunes the value of the former state to the latter one. Such update is called ``back-up'', and the difference between the values of two states is called ``Temporal Difference'', hence the name TD learning. An update has the form: 
%\begin{equation}
%\val(\state_t) =  \val(\state_t) + \rlLearn \Delta\val
%\label{eqRLValAll}
%\end{equation}
%where $\rlLearn$ is learning rate as before and $\Delta\val$ is the TD term. In one-step TD, 
%\begin{equation}
%\Delta\val = \cost(\state_t,\action_t) + \dis\val(\state_{t+1}) - \val(\state_t)
%\label{eqTDTerm}
%\end{equation}
%
%Equation \eref{eqTDTerm} corresponds to only a single control action. If $n$ actions are performed (corresponding to a sequence of states covered), we have $n$-step TD. For easier notation manipulation, let $\cost_{t+1}$ be the cost of taking an action at $\state_t$ to jump to $\state_{t+1}$, the general $n$-step TD is: 
%\begin{equation}
%\Delta\val = \cost_{t+1} + \dis\cost_{t+2} +...+ \dis^{n-1}\cost_{t+n} + \dis^n \val(\state_{t+n}) - \val(x_t).
%\label{eqTDTermGeneral}
%\end{equation}
%
%There are many problems where the model that generates the dynamics of the system is known. The model may be comprised of many components, which makes computing the transition probability for the entire state space difficult. However, since the dynamics of the model is known, it is easy to simulate the system. TD can exploit this property. Instead of updating only visited states, the controller can simulate the system ahead, and update states based on its simulated visited states and adjust the state values \emph{before} making a decision.


%We have treated the policy iteration: equation \eref{eqRLVal} in model-free ground is equivalent to \eref{eqStatePolIter} in model-based ground. For the case of value iteration, unfortunately, we do not have a set of actions to choose from, as in equation \eref{eqStateValIter}, which is repeated here
%
%\begin{equation}
%\val_i(\state)=  \min_{\ctrl \in \ctrlSet} \Bigl\{ {\cost(\state,\ctrl) + \dis E \{ \val_{i-1}(\state')\} \Bigr\} } .
%\end{equation}
%
%Instead, 

\section{ADP}
DP is a complete algorithm to solve an MDP; the policy found will converge to the optimal policy. However, only small problems with tens of thousands of states can be solved in practical time due to the curse of dimensionality. For many real-life problems, a suboptimal solution needs to be produced in a practical timeframe instead of an optimal, but late, solution. For example, vehicles at an intersection cannot wait on the traffic light for a day. In addition to dimensionality, there are also problems with changes in data. For example,  new vehicles may join a queue after a solution is produced and, if DP is being used, then the whole solution process must be restarted.

Under such circumstances, we have to strike a reasonable balance between optimality and efficiency, and that gives rise to ADP, a suboptimal control algorithm. There are many ADP algorithms, but we just describe two of them that we use together in this work.

\subsection{Roll-out ADP}
One simple way to reduce the computation of DP is to limit the horizon to some look-ahead states. Increasing the number of look-ahead states quickens the convergence, but also increases the amount of computation. In one-step look-ahead, the algorithm assumes the value of the next state is known by, e.g., some approximation. Let $\tilde{J}(\cdot)$ denote the converged approximate value of a state, then the value of current state is computed, approximately, as follows: 

\begin{equation}
\bar{\val}(\cstate)=  \min_{\ctrl \in \ctrlSet} \Bigl\{ {\cost(\cstate,\ctrl) + \dis E \{ \tilde{\val}(\nstate)\} \Bigr\} } ,
\label{eqStateValOneStepLook}
\end{equation}
and the control action chosen to minimise of the right-hand side of the equation. $\tilde{J}(\cdot)$ is assumed to be the value at infinite horizon.

We may have any number of look-ahead steps. In a two-step look-ahead algorithm, for example, equation \eref{eqStateValOneStepLook} is extended to: 

\begin{equation}
\bar{\val}(\cstate)=  \min_{\ctrl \in \ctrlSet} \Bigl\{ {\cost(\cstate,\ctrl) + \dis E \{ \min_{\ctrl_{t+1} \in \ctrlSet} \{\cost(\nstate,\ctrl_{t+1}) + \dis E \{\tilde{\val}(\state_{t+2}) \} \}   \} \Bigr\} } .
\label{eqStateValTwoStepLook}
\end{equation}

However, there are limits to the roll-out algorithm. The number of minimisation steps in a roll-out algorithm is exponential in the number of look-ahead steps, which is still a computational burden. The number of look-ahead steps must be limited for the algorithm to be tractable. Another way to remedy this problem is to limit the decision set to promising decisions ($\bar{\ctrlSet}$ instead of $\ctrlSet$) only. By limiting the number of decision choices, the algorithm can span more look-ahead steps while remaining tractable. In traffic scheduling, the transition is highly volatile and, therefore, one-step look-ahead may introduce more noise than the multi-step counterpart. Hence, we favour the multi-step approach in this work and denote it \emph{M-step scheduling} for later reference.

\subsection{M-step scheduling and simulated trajectories}
In this section, we use $g$ to represent the cost of moving from one state to another by a certain action. For example, the cost of transitioning from  $\state_k$ to $\state_{k+1}$, without interest in the action taken, is denoted $\stepcost(\state_k, \state_{k+1})$. 

At current time $t$, ADP computes the minimum cost to reach $t+M$: 
\begin{multline}\label{eqC1JBarM}
\bar{\val}_M(\state_t) = \min_{\ctrl_t\in \bar{\ctrlSet}} E\Bigl[ \sum^{t+M-1}_{k=t} \dis^{k-t}\stepcost(\state_k, \state_{k+1}) \\+ \dis^M \tilde{\val}_{t-1}(\state_{t+M-1}) \Bigr], t=0,...,\infty,
\end{multline}
and chooses the optimal decision by:
\begin{equation}\label{eqC1ArgminU}
\ctrl^\ast_t = \arg\min_{\ctrl_t\in \bar{\ctrlSet}} E \Bigl[ \sum^{t+M-1}_{k=t} \dis^{k-t}\stepcost(\state_k, \state_{k+1}) + \dis^M \tilde{\val}_{t-1}(\state_{t+M-1}) \Bigr].
\end{equation}
The subscript of $\tilde{\val}$ means that the approximation depends on information at the previous time, $t-1$.
In equations \eref{eqC1JBarM} and \eref{eqC1ArgminU}, the decision set is now limited to $\bar{\ctrlSet}$. The limited decision set is the combination of decisions along simulated control trajectories. First, trajectories at the boundaries of solution space are generated.  Next, other feasible trajectories are generated randomly using Monte Carlo.  A 
Monte Carlo simulation is also used to model future states for the expectation in equations \eref{eqC1JBarM} and \eref{eqC1ArgminU}. So the `optimal' decision, the least costly decision among the simulated decisions, is found by: 
\begin{multline}\label{eqC1OptimStepRollOut}
\tilde{u}^\ast_t = \arg\min_{u_t\in U} \{ \sum^{t+M-1}_{k=t} \dis^{k-t}\stepcost(\state_k, \state_{k+1}) \\+ \dis^M \tilde{J}_{t-1}(\state_{t+M-1}) \}, \text{ for } t=0,...,\infty.
\end{multline}
%After $M$ steps of transition, the cost incurred is:
%\begin{multline}\label{eqC1JHatM}
%\hat{\val}_M = \min_{u_t\in U} \{ \sum^{t+M-1}_{k=t} \dis^{k-t}g(\state_k, \state_{k+1}) \\+ \dis^M \tilde{J}_{t-1}(\state_{t+M-1}) \}, \text{ for } t=0,..,\infty
%\end{multline}

\subsection{Parametric ADP}
\label{secChap1ADP}
To overcome the curse of dimensionality, approximation functions are used to store the state space instead of a tabular form, as is done in DP.  The approximation structure is: 
\begin{equation}
\tilde{J}(\state,\paramVec) := \approxFunc(\state)\paramVec,
\label{eqApproxStateVal}
\end{equation}
where $\approxFunc(\cdot)$ is a function vector and $\paramVec$ is a weight column vector, weighting the importance of the features. More specifically, equation \eref{eqApproxStateVal} can be written:
\begin{equation}
\tilde{J}(\state,\paramVec) := \sum_{n=1}^{N} \approxCompo_n(\state) \paramVec(n),
\label{eqApproxStateValBreak}
\end{equation}
where $N$ is the size of the feature vector and $r(n)$ is component $n$ of vector $r$. Each component function $\approxCompo_n(\cdot)$ can be linear or nonlinear in $\state$.  If the approximation structure remains linear in $\approxCompo$ and $\approxCompo(\cdot)$ is monotonic, then convergence is guaranteed~\citep{Bertsekas2007}, albeit the converged policy may be suboptimal.  With the parameter-based approximation, we can perform policy improvement in the same way as DP: replacing $J(\cdot)$ by the approximation $\tilde{J}(\cdot,\cdot)$. Equation~\eref{eqStatePolImprov} is now re-formulated:
\begin{equation}
\mu^{\pi+1}(\cstate,\paramVec^{\pi})=  arg\min_{\ctrl \in \ctrlSet} \Bigl\{ {\cost(\cstate,\ctrl) + \dis E \{ \tilde{\val}^{\pi}(\nstate,\paramVec^{\pi})\} \Bigr\} },
\label{eqStatePolImprovApprox}
\end{equation}
where $\paramVec^{\pi}$ denotes the parametric vector under policy $\pi$. Hence, the new policy is based on the old value of the parametric vector.  Now, the learning is not performed on a state, but instead on the parametric vector. Let $\action_t^{\pi+1}$ be the action under new policy $\pi+1$, and let $\nstate^{\pi+1}$ be the following state if the action is carried out. The gradient descent method is used to update the parametric vector as follows:

\begin{equation}
\label{eqRLValApprox}
\paramVec_n^{\pi+1}  =  \paramVec_n^\pi + \\ \rlLearn(\cost(\cstate,\action_t^{\pi+1}) + \dis \valApprox(\nstate^{\pi+1},\paramVec^{\pi}) - \valApprox(\cstate,\paramVec^{\pi})) \frac{\partial \valApprox(\cstate,\paramVec^\pi)}{\partial r_n^{\pi}}, 
\end {equation}

\begin{equation}
\label{eqRLValApproxSimp}
\paramVec_n^{\pi+1} =  \paramVec_n^{\pi} + \rlLearn(\cost(\cstate,\action_t^{\pi+1}) + \dis \valApprox(\nstate^{\pi+1},\paramVec^\pi) - \valApprox(\cstate,\paramVec^\pi)) \approxCompo_n .
\end {equation}
To guarantee convergence, $\rlLearn$ must satisfy the conditions: 
\begin{equation}
\sum_{0}^\infty\rlLearn = \infty \text{ and }  \sum_{0}^\infty\rlLearn^2 < \infty.
\label{eqC1EtaCond}
\end{equation}
A simple diminishing rate $\rlLearn_t = 1/t$ is sufficient. The update process in \eref{eqRLValApproxSimp} is equivalent to the TD(0) algorithm~\citep{Sutton1998} performed on the parametric vector.

By storing the value of the parametric vectors instead of the state space itself, ADP significantly compresses the state space. However, the more significant impact is the generalisation. In exact DP, the value of a state is unknown if the state has not been visited. Exact DP does not capture the correlations between the states. In ADP, the controller learns the value of the parametric vector from the visited states, but retains the values for whichever state that yields the same feature, as obtained from the feature function. Hence, ADP can make more informed decisions than DP.

Monte Carlo simulation can be used to learn the values of $\paramVec$. When simulation is used, the expected state is replaced by the sampled state; see Figure \ref{ApproxPolIterLoop}. Algorithm \ref{alApproxIter} describes the process.
%\begin{table}
\begin{algorithm}[ht]
\floatname{algorithm}{Algorithm}
\caption[Approximate Policy Iteration]{\label{alApproxIter}Approximate Policy Iteration. Stopping condition is empirically selected. For example, after certain number of iterations }
\begin{algorithmic}[1]
\STATE Starting with state $\cstate$, current policy $\mu^\pi$
\STATE Initializing $\paramVec$
\STATE \textit{decision generator} generates some candidate policies
\IF {samples of next state $\nstate$ not exists}
	\STATE \textit{system simulator} generate samples
\ENDIF
\WHILE{stopping condition not met}
\STATE \textit{cost approximator} generates cost for sampled states $\nstate$
\STATE based on previously generated samples, \textit{decision generator} chooses improved policy according to equation \eref{eqStatePolImprovApprox}
\STATE \textit{system simulator} simulates the system with the improved policy $\mu^{\pi+1}$, new samples are collected.
\STATE \textit{cost approximator} update $\paramVec$ according to equation \eref{eqRLValApproxSimp}
\STATE choose another state to used as $\cstate$
\ENDWHILE
\end{algorithmic}
\end {algorithm}
%\end{table}

\begin{figure*}[htb]%
\includegraphics[width=4.5in]{ApproxPolIterLoop2}%
\caption{Approximate policy iteration work flow. Redrawn from~\citep{Bertsekas2011}.}%
\label{ApproxPolIterLoop}%
\end{figure*}

\subsection{Combination of roll-out and parametric forms of ADP}
Under the parametric form of ADP outlined in Section~\ref{secChap1ADP}, the algorithm performs a one-step look-ahead to find the after-transition state $\nstate$, which is used to approximate the cost. Similarly, we can apply M-step look-ahead and approximate the cost at the end of the M-step period:
\begin{multline}\label{eqC1JHatM}
\hat{\val}_M = \min_{u_t\in U} \{ \sum^{t+M-1}_{k=t} \dis^{k-t}g(\state_k, \state_{k+1}) \\+ \dis^M \tilde{J}_{t-1}(\state_{t+M-1},r_{t-1}) \}, \text{ for } t=0,...,\infty.
\end{multline}
As noted before, the subscript $t-1$ of $\tilde{\val}$ and $r$ means that the approximation is based on the value of $r$ at $t-1$. The `optimal' decision, i.e., the least costly among the simulated trajectories,  is found by

\begin{multline}\label{eqC1UArgminApprox}
\tilde{u}^\ast_t = \arg\min_{u_t\in U} \{ \sum^{t+M-1}_{k=t} \dis^{k-t}g(\state_k, \state_{k+1}) \\+ \dis^M \tilde{J}_{t-1}(\state_{t+M-1},r_{t-1}) \}, \text{ for } t=0,...,\infty.
\end{multline}
Let $\Delta$ denote the ``Temporal Difference'' (TD) between two consecutive steps, and let $k$ be the step. Both $k$ and $t$  represent a time step: $k$ represents the time step during M-step simulation process, and $t$ represents the universal time step.
%To avoid the clash of notation (between vector component and time step), let $\paramVec(k)$ 
%%and $\mu_k$ 
%denote the parametric vector available at step $k$. When we use $t$ in the subscript, it means time, for e.g, $\paramVec_{t-1}$ means parametric vector at universal time step $t-1$, and $\paramVec_t$ parametric vector at universal time step $t$.
%and improved policy at step $k$, respectively. 
The TD between two consecutive steps is:
\begin{equation}
\Delta_k(\state_k, \state_{k+1}) = g(\state_k,\state_{k+1}) + \dis \tilde{\val}(\state_{k+1},\paramVec_k) - \tilde{\val}(\state_k,\paramVec_k),  k = 0,...,M-2.
\label{eqTDoneStep}
\end{equation}
Let $r_k = r_{t-1}$ for $k=0$.
The TD update is computed on the go as follows:
%\begin{equation}
%\paramVec(k+1) = \paramVec(k) + \rlLearn_k \Delta_k \sum_{t=0}^k{(\lambda\dis)^{k-t}\delta_{\state_k}\nabla\tilde{J}(\state_t,\paramVec(t))}
%\label{eqUpdateR}
%\end{equation}
\begin{equation}
\paramVec_{k+1} = \paramVec_k + \rlLearn_k \Delta_k \sum_{i=0}^k{\dis^{k-i}\nabla\tilde{J}(\state_i,\paramVec_i)} \hspace{1cm} \text{for } k=0,...,M-1.
\label{eqUpdateR}
\end{equation}
This update process is TD(1), a special case of TD($\lambda$) algorithm~\citep{Sutton1998}.
Recall that 
\begin{equation}
\tilde{J}(\state,\paramVec) := \sum_{n=1}^{N} \approxCompo_n(\state) \paramVec(n), \hspace{1cm}  n = 1,...,N,
\label{eqApproxStateValBreakRecall}
\end{equation}
and that by taking the derivative with respect to the component $r(n)$, we have 
%\begin{equation}
%\paramVec(k+1)_n = \paramVec(k)_n + \rlLearn_k \Delta_k \sum_{t=0}^k{(\lambda\dis)^{k-t}\delta_{\state_k}\approxCompo_n(\state_t)}
%\label{eqC1UpdateR2}
%\end{equation}
\begin{equation}
\paramVec_{k+1} (n) = \paramVec_k(n) + \rlLearn_k \Delta_k \sum_{i=0}^k{\dis^{k-i}\approxCompo_n(\state_i)},  \hspace{1cm} \text{for } k=0,...,M-1.
\label{eqC1UpdateR2}
\end{equation}
After an M-step update,  the universal $r$ vector is updated as follows
\begin{equation}
r_t = r_M.
\label{eqC1UpdateR3}
\end{equation}
%Let $\Delta_M$ denote the M-step Temporal Difference, it is computed by 
%\begin{equation}
%\Delta_M=\hat{J}_M-\tilde{\val}(\state_t,r_t)
%\label{}
%\end{equation}
%The parametric vector is updated by: 
%\begin{equation}
%r_t = r_{t-1} + \eta_t \nabla_r \Delta_M
%\label{eqC1UpdateR1}
%\end{equation}


\section{Summary}
This chapter laid the theoretical background for making sequential decisions and demonstrated how such a problem is formulated formally as an MDP. The exact solution to an MDP can be achieved by using exact DP. 
However, the state space for real-world problems often makes DP too computationally demanding.  ADP, a suboptimal control method, can be used instead to circumvent this difficulty. Despite being suboptimal, ADP is often used produce  a solution with a reasonable trade-off between optimality and speed. 

There are many approaches to compute the solution to an ADP problem. In particular,  we use a roll-out algorithm and a parametric representation. In the roll-out algorithm, the look-ahead is cut off at some steps to reduce computation. With a parametric representation, the state values are not stored in a tabular form, but computed via a feature function and a parametric vector. Such a representation not only saves memory but also facilitates generalisation. In the next chapter, we will see how these algorithms are applied to solve the problem of scheduling traffic lights.

% ------------------------------------------------------------------------
\end{spacing}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../thesis"
%%% End:
