%\def\baselinestretch{1}
\chapter{Conclusion and future research directions}\label{chapClus}
\ifpdf
    \graphicspath{{Conclusions/ConclusionsFigs/PNG/}{Conclusions/ConclusionsFigs/PDF/}{Conclusions/ConclusionsFigs/}}
\else
    \graphicspath{{Conclusions/ConclusionsFigs/EPS/}{Conclusions/ConclusionsFigs/}}
\fi

\begin{spacing}{1.5}
%\def\baselinestretch{1.66}
From a simple gas-lamp traffic signal controller installed in London in 1868\footnote{Which later exploded and injured the operating officer.} to the responsive controllers of today, traffic control systems have come a long way. However, pressing road traffic demands constantly drives improvement. In this thesis, we make contributions to this field.

Specifically, this thesis proposes an adaptive signal-vehicle cooperative control method in which both traffic signals and vehicles' speeds are controlled effectively online. The controller aims at reducing travel delay, fuel consumption and \carbon\ emissions. In the control scheme, two-way V2I communication is employed. Equipped vehicles, called smart cars, broadcast their positions to give the controller their exact real-time positions. The controller sends out suggested speeds to equipped vehicles so that they get through the downstream intersection during the green light. If equipped vehicles cannot get through the intersection during the green light, then at least they do not waste fuel on accelerating-then-braking manoeuvres. All the tasks for scheduling traffic signals and computing suggested speeds are done in real time.

The most striking difference between our proposed control method and current state-of-the-art methods is that our method does not separately control traffic signals and vehicles' speeds. Instead, it controls both. Throughout this thesis, we have shown evidence which supports the set of hypotheses set out in the beginning of this work. They can be summarised as follows.

\section{Summary}
In Chapter \ref{chap2}, we propose an adaptive \emph{signal} controller based on ADP. There is no speed control scheme in this controller. The controller is evaluated in various traffic scenarios. The scenarios differ in the number of intersections, link length and traffic demands. Simulation results showed that our system outperformed TRANSYT substantially in terms of delay and stops. %In the chapter, we created six test scenarios. 
%Simulated results shows that our control gain over TRANSYT is the least (4\% gain in objective value) when the traffic demand is light and fixed, and the link length between two intersections is 200 meters. In that scenario, the delay is saved by more than 16\% while the stops is punished by more than 5\%. On the other hand, the saving is peak when the traffic demand is heavy and fixed, and the network is composed of 6 intersections placed on an arterial road, each 200 meters away from the others. In that scenario, the delay is saved by more than 69\%, stops 62\%, and objective value 66\%. 
This confirms grand hypothesis one, that our system is more effective than a good fixed-time controller even when there are no smart cars in the system.

In Chapter \ref{chapSpeedAdvising}, we propose a simulation-based speed advising system. Signal schedules are produced using TRANSYT and kept fixed. The controller produces suggested speeds for smart cars so that they reach the intersection when there is green light and no queue. If this is not feasible, then the suggested speeds are set to a minimum level to keep the cars moving. The controller employs a simulation process to find suitable suggested arrival times. The controller does not control non-smart cars, i.e., non-equipped cars. The controller is implemented on a complex grid network of four intersections. Simulation results show that when the percentage of smart cars increases, the fuel consumption and \carbon\ emissions are reduced. With a 100\% smart-car rate, the saving in fuel consumption and \carbon\ emissions are 8.2\% and 6.9\%, respectively. This confirms the effectiveness of the speed control method we devised.

In Chapter \ref{chap4}, we propose our ultimate system, \cName, based on the signal control method in Chapter \ref{chap2} and the speed advising system in Chapter \ref{chapSpeedAdvising}. The controller is implemented on a complex network, as in Chapter \ref{chapSpeedAdvising}. When there are no smart cars in the system, our control system outperforms TRANSYT-based fixed-time in all measures of effectiveness: delay, stops, fuel consumption and \carbon\ emissions. This again confirms grand hypothesis one. When the smart-car rate increases, the savings in all measures of effectiveness increase with the smart-car rate. At a 30\% smart-car rate, delay starts to decrease. At a 4\% smart-car rate, fuel consumption and \carbon\ emissions begin decreasing. At a 100\% smart-car rate, the savings in fuel consumption and \carbon\ emissions are 11.43\% and 12.77\%, respectively. The savings in delay, stops and objective value are 36.62\%, 7.9\% and 24.41\%, respectively. This confirms grand hypothesis two that cooperative control is more effective than \emph{signal-only} control, even if not all cars are smart cars.

%Particularly, delay in the system with smart cars only is saved nearly 14\%, compared to the system with no smart cars at all. The saving in stops is not as high as delay, with 1.8\% saving. That results in a saving of 8\% objective value (with $k=40$). As for fuel consumption, when all cars are smart cars, the saving is 5.78\% compared to when all cars are non-smart cars. The counterpart figure for \carbon\ emission is 6.77\%. These statistics confirm our grand hypothesis two that the fully cooperative control  scheme, in which vehicles' speeds are controlled, is more effective than a \emph{signal} control.

%In the chapter, we also compared \cName\ to \emph{Fplus controller} (TRANSYT-based fixed time controller) at every point of smart-car rate, i.e., \cName\_1\% to Fplus\_1\%, \cName\_2\% to Fplus\_2\% and so on. The saving in delay is in the range 25-35\%, stops 1-6\%, fuel consumption 3-6\%, \carbon\ emission 5-6.5\%. %This confirms our grand hypothesis three that fully cooperative control is more effective than \oneway\ cooperative control. 

Chapters \ref{chap1a} and \ref{chap1} lay out the literature and the theoretical framework used in this thesis, respectively.

\section{Future works and research directions}
\subsection{Using a continuous car-movement model}
If we use a continuous car-following model, e.g., \citep{Treiber2000}, where acceleration (deceleration) can have any real value, then the effect on reducing fuel consumption and \carbon\ emissions may be larger; see Section \ref{secC3Limit}.  

\subsection{Extending network size}
In this thesis, the controller is built for a single intersection. Although it has successfully controlled multiple intersections in a decentralized way, its performance can still be improved by extending the communication scope. For example, smart cars can communicate with two or three intersections downstream to plan their speeds ahead. A coordination method between intersections can also be set up. The coordination can be done by using a hierarchical approach, e.g., as in the RHODES system \citep{Head01}. With smart cars in the systems, precise timings for them to arrive at intersections can be achieved. Then coordination then can be more effective, e.g., the cut-off point of a phase is more precise, leaving no vehicles on an arterial road stranded.

\subsection{Bridging signalized intersection and completely non-signalized intersection}
As we reviewed in Chapter \ref{chap1a}, some authors have proposed autonomous intersection management schemes \citep{Stone2008, Lee2011}. They assume that all vehicles are autonomous, i.e., self-driving. While the current technology trends show that this is a tangible goal in the not-so-distant future,  some schemes should be devised to bridge the current state to that point. For example, we can think of a semi-signalized intersection. Under that scheme, there are signal phases for human-driven cars and special signal phases when autonomous cars can cross one another without stopping.

A more realistic control method can be applied is relaxing the roads' maximum speeds for smart cars. They have to abide to a limit speed, but this limit is higher than normal cars. Safety is less of a concern because a smart car's speed is always under control.

%\section{Discussions}
%In Section\ref{secDiscussion} of the Chapter\ref{chap4}, we discussed the significance of our method in relating to methods by other authors. In this section, we would like to discuss its significance in a broader sense. Simulated results show that our system offer a saving of more than 11\% fuel consumption and 12\% \carbon\ emission compared to Fplus controller, which is a TRANSYT-based controller. If the simulated results truely reflect the real world, it is a significant saving since SCOOT only saves 5\% of fuel consumption in a site in Worcester, UK \citep{SCOOTWorcester}. 12\% \carbon reduction is also a significant number if we put in the scale with 


%\subsection{Real-life significance }
%If we put our results in saving \carbon\ in context with the recently announced controversial Carbon Tax scheme in Australia, the significance is clear. In an effort to curb \carbon\ emission in 2020 by only 5\%, compared to the the year 2000's level, Australian government applies a Carbon Tax scheme to raise at least A\$10 billion. 
%
%In other events, the comments we often receive when working towards this thesis is whether the savings in fuel consumption (which translate to monetary cost) are worthwhile to equip the entire fleet with the required technologies such as V2I and ACC. Our answer to that is that V2I is developing fast, and it is useful for many other application such as accident prevention and in-car entertainment (streaming from roadside devices to in-car devices) \citep{FordConnectedCar}. So fuel consumption and \carbon savings might not be the main driving force behind deploying V2I technology, but when it is ubiquitous, there is at least a system ready to tackle the problem: our system. 
  
%%% ----------------------------------------------------------------------
\end{spacing}
% ------------------------------------------------------------------------

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../thesis"
%%% End:
